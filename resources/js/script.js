'use strict';
var _game = null,
    _main = {},
    _preload = {};

_preload.boot = function() {
    this.firstRunPortrait = false;
};

_preload.boot.prototype = {
    init: function() {
        this.input.maxPointers  = 1;
        this.scale.scaleMode    = Phaser.ScaleManager.SHOW_ALL;
        this.scale.setMinWidth  = 315;
        this.scale.setMinHeight = 200;
        this.scale.setMaxWidth  = 1170;
        this.scale.setMaxHeight = 200;
        this.time.desiredFps    = 50;
        this.scale.pageAlignHorizontally   = true;
        this.scale.pageAlignVertically     = true;
        this.stage.disableVisibilityChange = true;
        this.stage.backgroundColor = '#6da2af';
        this.transparent    = true;
        this.stage.smoothed = true;
    },

    preload: function() {
        this.load.path = 'assets/';
        this.load.json('assets', 'assets.json', _game.rnd.integer());
    },

    create: function() {
        _game.global.assetsJSON = _game.cache.getJSON('assets');
    },

    update: function() {
        _game.state.start('Preloader');
    }
};

_preload.preloader = function() {
    this.preloadText = null;
};

_preload.preloader.prototype = {
    init: function() {
        this.preloadText = this.add.text(_game.world.centerX, _game.world.centerY , 'Cargando: 0%', { align: 'center', font: '20px Arcade', fill: '#0000' });
        this.preloadText.anchor.x = 0.5;
        this.preloadText.anchor.y = 0.5;
    },

    preload: function() {
        this.load.path = 'assets/';

        var sprites = _game.global.assetsJSON.sprites,
            audio   = _game.global.assetsJSON.audio,
            images  = _game.global.assetsJSON.images,
            cache   = '?' + Math.random().toString();

        if ( audio.length ) {
            for ( var a = audio.length - 1; a >= 0; a-- ) {
                this.load.audio(audio[a].name, audio[a].path + cache);
            }
        }

        if ( images.length ) {
            for ( var i = images.length - 1; i >= 0; i-- ) {
                this.load.image(images[i].name, images[i].path + cache);
            }
        }

        if ( sprites.length ) {
            for ( var s = sprites.length - 1; s >= 0; s-- ) {
                var width  = parseInt(sprites[s].width),
                    height = parseInt(sprites[s].height);

                this.load.spritesheet(sprites[s].name, sprites[s].path + cache, width, height);
            }
        }

        this.load.onFileComplete.add(this.fileComplete, this);
        this.load.onLoadComplete.add(this.fileComplete, this);
    },

    loadComplete: function() {
        this.load.onFileComplete.add(this.fileComplete, this);
        this.load.onLoadComplete.add(this.fileComplete, this);
    },

    fileComplete: function(progress) {
        this.preloadText.text = 'Cargando '+ progress +'%';
    },

    loadUpdate: function() {

    },

    create: function() {
        this.state.start('Home');
    }
};

_main.christmas = function() {
    this.config = {
        'background'   : null,
        'controls'     : null,
        'play'         : false,
        'time'         : 0,
        'timeRange'    : [ 1000, 4500 ],
        'timeControls' : 0,
        'acceleration' : 15,
        'speed'        : 75,
        'maxSpeed'     : 1000,
        'isJump'       : true
    },
        this.texts = {
            'endGame' : null,
            'hi'      : null,
            'score'   : null,
            'defaultScore' : {}
        },
        this.tree      = null,
        this.treeItems = [],
        this.player    = null,
        this.mountains = {
            'sprite'   : null,
            'speed'    : 1,
            'maxSpeed' : 15,
            'acceleration' : 0.5
        },
        this.snow       = null,
        this.score      = 0,
        this.scoreTotal = 0,
        this.floor      = null,
        this.btnPlay    = null,
        this.btnReload  = null
};

_main.christmas.prototype = {

    init: function() {
        this.config.play = false;
        this.treeItems   = [ 0, 1, 2 ];
        this.texts.defaultScore = { font: "16px Arcade", fill: "#3c3c3c", align: "right" };

        this.physics.startSystem(Phaser.Physics.NINJA);
        this.physics.ninja.setBoundsToWorld();
        // this.physics.ninja.gravity = 1;
    },

    showResetConfig: function() {
        this.config.time   = 0;
        this.config.speed  = 75;
        this.score         = 0;
        this.config.isJump = true;
        this.config.play   = true;
    },

    showPlay: function() {
        this.player.animations.play('run');
        this.btnPlay.destroy();

        this.texts.score = _game.add.text(_game.world.width - 100, _game.world.top + 5, 'SCORE  0', this.texts.defaultScore);
        this.texts.hi    = _game.add.text( ( this.texts.score.x - this.texts.score.width ) - 5, _game.world.top + 5, 'HI  0', this.texts.defaultScore);
        this.config.play = true;
    },

    showReload: function() {
        if ( this.scoreTotal <= this.score ) {
            this.scoreTotal = this.score;
        }

        this.texts.hi.text = 'HI  ' + this.scoreTotal;

        this.tree.destroy(true);
        this.player.animations.play('run');
        this.btnReload.destroy();
        this.texts.endGame.destroy();
        this.createTree();
        this.showResetConfig();
    },

    showControls: function() {
        if ( !this.config.play ) {
            if ( this.config.controls.isDown ) {
                this.showReload();
            }
        } else {
            if ( this.player.body.touching.down ) {
                this.config.isJump = true;
            }

            if ( this.config.controls.duration < 250 && this.config.controls.isDown && this.config.isJump ) {
                this.config.isJump = false;
                this.player.body.moveUp(240);
                this.player.animations.play('up');
            } else {
                this.player.animations.play('run');
            }

            if ( this.config.controls.duration < 250 && _game.input.activePointer.isDown && this.config.isJump ) {
                this.config.isJump = false;
                this.player.body.moveUp(250);
                this.player.animations.play('up');
            }  else {
                this.player.animations.play('run');
            }
        }
    },

    createNutcracker: function() {
        this.player = _game.add.sprite(200, _game.world.bottom - 47, 'nutcracker');
        this.physics.ninja.enable(this.player);

        this.player.body.bounce   = 0.1;
        this.player.body.friction = 0.5;
        this.player.body.collideWorldBounds  = true;
        this.player.body.checkCollision.up   = true;
        this.player.body.checkCollision.down = true;
        this.player.body.checkCollision.left = false;
        this.player.anchor.setTo(0.5, 0.5);

        this.player.animations.add('up', [ 0 ], 15, false);
        this.player.animations.add('run', [ 1, 2 ], 10, true);
    },

    createFloor: function () {
        var widthBgSeleted  = this.game.width,
            heightBgSeleted = 24;

        var bmd = this.add.bitmapData(widthBgSeleted, heightBgSeleted);
        bmd.ctx.fillStyle = '#9ed4d5';
        bmd.ctx.beginPath();
        bmd.ctx.rect(0, 0, widthBgSeleted, heightBgSeleted);
        bmd.ctx.fill();

        this.floor = this.add.sprite(0, this.game.height - heightBgSeleted, bmd);
        this.physics.ninja.enableTile(this.floor, 1);
    },

    createTree: function() {
        this.tree                 = _game.add.group();
        this.tree.name            = 'tree';
        this.tree.enableBody      = true;
        this.tree.physicsBodyType = Phaser.Physics.NINJA;
        this.tree.createMultiple(1, 'tree');
        this.tree.setAll('outOfBoundsKill', true);
        this.tree.setAll('body.collideWorldBounds', true);
    },

    createSnow: function () {
        this.snow                 = _game.add.group();
        this.snow.name            = 'snow';
        this.tree.enableBody      = true;
        this.tree.physicsBodyType = Phaser.Physics.NINJA;
        this.tree.createMultiple(1, 'snow');
        this.tree.setAll('outOfBoundsKill', true);
        this.tree.setAll('body.collideWorldBounds', true);
    },

    impactTree: function(player, tree) {
        this.player.animations.stop();
        this.updateTreeSpeed(true);

        this.btnReload     = _game.add.button(_game.world.centerX, _game.world.centerY, 'reload', this.showReload, this, 2, 1, 0);
        this.texts.endGame = _game.add.text(_game.world.centerX - 15, _game.world.centerY - 35, 'END GAME', { font: "20px Arcade", fill: "#3c3c3c" });
        this.config.play   = false;
    },

    generateTree: function() {
        var item     = this.treeItems[_game.rnd.integerInRange(0, this.treeItems.length - 1)],
            height   = _game.world.height,
            typeTree = null,
            interval = 2150,
            tree     = null;

        switch ( item ) {
            case 0: typeTree = 'big';    height = height - 71; break;
            case 1: typeTree = 'double'; height = height - 55; break;
            case 2: typeTree = 'small';  height = height - 55; break;
        }

        tree = this.tree.create(_game.width + 60, height - 30, typeTree);
        this.physics.ninja.enable(tree);

        tree.body.bounce      = 0;
        tree.body.friction    = 0;
        tree.checkWorldBounds = true;
        tree.body.collideWorldBounds = false;
        tree.body.moveLeft(this.config.speed);

        tree.events.onOutOfBounds.add(function(item) {
            item.filters = null;
        },this);

        tree.events.onKilled.add(function(item) {
            item.filters = null;
        }, this);

        if ( this.score > 500 ) {
            interval = 1750;
        } else if ( this.score > 1000 ) {
            interval = 1250;
        } else if ( this.score > 1500 ) {
            interval = 750;
        } else if ( this.score > 2000 ) {
            interval = 250;
        }

        if ( interval < this.config.speed ) {
            interval = this.config.speed + 350;
        }

        this.config.time = _game.time.now + interval - this.config.speed;
    },

    generateSnow: function() {
        var randomX = _game.rnd.integerInRange(0, _game.width),
            snow    = this.snow.create(randomX, - 100, 'snow');

        this.physics.ninja.enable(snow);

        snow.body.bounce      = 1;
        snow.body.friction    = 0;
        snow.body.maxSpeed    = 0.5;
        snow.body.gravity     = false;
        snow.checkWorldBounds = true;
        snow.body.collideWorldBounds = false;

        snow.events.onOutOfBounds.add(function(item) {
            item.filters = null;
        },this);
    },

    updateSpeed: function() {
        if ( this.config.speed < this.config.maxSpeed ) {
            this.config.speed += this.config.acceleration;
        }

        if ( this.mountains.speed < this.mountains.maxSpeed ) {
            this.mountains.speed += this.mountains.acceleration;
        }

        this.updateTreeSpeed(false);
    },

    updateTreeSpeed: function(stop) {
        this.tree.forEach(function(children) {
            if  ( stop ) {
                children.kill();
            } else {
                children.body.moveLeft(this.config.speed);
            }
        }, this)
    },

    create: function() {
        this.config.controls   = _game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.config.background = _game.add.tileSprite(0, 0, _game.stage.width, _game.cache.getImage('gameBackground').height, 'gameBackground');
        this.btnPlay           = _game.add.button(_game.world.centerX, _game.world.centerY, 'play', this.showPlay, this, 2, 1, 0);

        this.mountains.sprite = _game.add.tileSprite(0, _game.height - _game.cache.getImage('mountains').height - 24, _game.stage.width, _game.cache.getImage('mountains').height, 'mountains');
        this.mountains.sprite.scale.set(1.13)

        this.createNutcracker();
        this.createTree();
        this.createSnow();
        this.createFloor();
    },

    update: function() {
        _game.physics.ninja.collide(this.tree, this.floor);
        _game.physics.ninja.collide(this.player, this.floor);

        this.showControls();

        if ( this.config.play ) {
            this.mountains.sprite.tilePosition.x -= 1;

            this.generateSnow();

            if ( _game.time.now > this.config.time ) {
                this.generateTree();
                this.updateSpeed();
            }

            this.texts.score.text = 'SCORE  ' + this.score;
            this.score++;

            _game.physics.ninja.collide(this.tree, this.player, this.impactTree, null, this);
        }
    }
};

function resizeGame() {
    setTimeout(function() {
        $('#game').show();
        $('#game').width( $(window).width() );
        $('#game').height( $(window).height() );
    }, 200);
}

function initLoad() {
    _game = new Phaser.Game(1170, 200, Phaser.Auto, 'christmas', null, true);

    _game.global = {
        loaderFile : 'assets.json?' + Math.random(),
        assetsJSON : null,
        homeUtils  : {},
        iPoints    : '',
        uPoints: function() {
            console.log( 'uPoints' );
        }
    };

    _game.state.add('Boot',      _preload.boot);
    _game.state.add('Preloader', _preload.preloader);
    _game.state.add('Home',      _main.christmas);
    _game.state.start('Boot');

    resizeGame();
}

$(window).on('load', initLoad);